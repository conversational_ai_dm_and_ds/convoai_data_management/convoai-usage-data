def base_table_query(start_date, end_date=None): 
    query = f'''
    SELECT distinct
        x.CONVERSATION_TURN_KEY as conversation_turn_key,
        x.conversation_id, 
        x.CHAT_TURN_INDX_NBR as turn_index,
        x.USR_UTTERANCE_DTM as timestamp_of_user_text,
        CHAT_BOT_RESPNS_TXT as bot_response,
        USR_UTTERANCE_TXT as user_utterance, 
        x.BOT_CLARIFICATION_CNT as clarification_count,
        x.BEST_INTNT_NM as name,
        x.TRFRD_TO_AGENT_FLAG as transferred_to_live_agent, 
        x.ENGAGEMENT_ID as engagement_id,
        x.RESPNS_DTM as timestamp_of_bot_response,
        x.NEXT_SUGSTD_ACTN_CD as next_bot_action,
        x.TRANSF_REASON_CD as reason_for_transfer,
        x.CHAT_SENTIMENT_SCORE as sentiment_score,
        x.USER_CIF as user_id,
        x.TOTAL_REQUEST_TIME as total_request_time,
        x.EVENT_CD as event,
        BEST_INTNT_CONFD_SCORE as confidence, 
        CASE
        WHEN lower(substring(x.conversation_id,0,3)) = 'cli' or lower(substring(x.conversation_id,0,4)) = 'test'  THEN  'Testing'
        ELSE 'Normal'
        END as Bot_record_type,
        case 
        when initcap(CHNL_TYP_CD) = 'Mobile_sdk' or initcap(CHNL_TYP_CD) = 'Messaging' THEN 'Messaging' 
        else 'Chat'
        end as channel,
        x.LOAD_DTM as load_date,
        case when x.TRFRD_TO_AGENT_FLAG = false then 'Not Transferred' else 'Transferred' end as convo_level 
    FROM 
        TEAM_TECH_CONVOAI_P.CORE.V_DAILY_CHAT_LOGS x
    WHERE 
        case when lower(substring(x.conversation_id,3)) = 'cli' or lower(substring(x.conversation_id,4)) = 'test' THEN 'Testing' else 'Normal' end = 'Normal'
    and  (CAST(LOAD_DTM as DATE) >= CAST('{start_date}' as DATE))
    '''
    if end_date is not None: 
        query = query + f''' and  (CAST(LOAD_DTM as DATE) <= CAST('{end_date}' as DATE)) '''
    
    query = query + '''    ORDER BY conversation_turn_key;'''
    return query
def query_train_data(): 
    query = f'''
    SELECT * FROM TEAM_TECH_CONVOAI_P.CORE."tbl_dim_master_utterances"
    '''
    return query 
def query_report(): 
    query = f'''SELECT * FROM USER_SANDBOX_P.PZSW95."DAILY_REPORTING_DEPOSIT_TBL"
    
    '''
    return query

