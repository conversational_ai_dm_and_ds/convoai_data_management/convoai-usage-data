import os 
from pathlib import Path
import yaml
import glob
import traceback 
import re 
import base64
import requests
from io import StringIO
import pandas as pd

path = Path(__file__).parent
intent_list = {}
path = os.path.abspath(os.path.join(path, os.pardir))

def get_all_lookups(data_list): 
    lookup_list = {}
    for items in data_list: 
        for item in items['nlu']: 
            if 'lookup' in item.keys(): 
                text = item['examples']
                text = text.replace("- ", "")
                keywords = text.split("\n")  
                lookup_list[item['lookup']] = keywords
    return lookup_list
def get_intent_utterances(data_list):
    intent_list = {}
    for nlu_intents in data_list: 
        for item in nlu_intents['nlu']:
            if 'intent' in item.keys():
                text = item['examples']
                utterances = text.split("\n")  
                utter_list = []
                for utter in utterances:
                    if utter:
                        result = re.sub(r'\{.*?\}', '', utter[1:].strip()) 
                        result_2 = re.sub(r'\(.*?\)', '', result)
                        result_3 = result_2.replace('[','')
                        result_4 = result_3.replace(']','')
                        utter_list.append(result_4)
                intent_list[item['intent']] = utter_list
    return intent_list
def get_domain_intents(domain_intents):
    intents = []
    for d in domain_intents['intents']: 
        if (type(d) is dict): 
            ints = list(d.keys())
            intents = intents + ints
        elif (type(d) is str): 
            intents.append(d)
    return intents
def get_file_content(project, file_path, branch: str='main'): 
    f = project.files.get(file_path=file_path, ref=branch)

    file_content = base64.b64decode(f.content).decode("utf-8")
    file_content = file_content.replace('\\n', '\n')
    data_dict = yaml.safe_load(file_content)
    return data_dict 

def get_file_content_csv(project, file_path, branch: str='main'): 

    print(file_path)
    # Reading URL, decoding and converting it to pandas dataframe using read_csv
    df = pd.read_csv(StringIO(requests.get(file_path).text), encoding="utf-8")
    # Printing the first 5 rows of the dataframe
    df.head()

    return df 

def get_all_yml_files(project, folder_path, branch: str='main'): 
    items = project.repository_tree(path=folder_path, ref='main')
    file_list = []
    data_list = []
    for x in items: 
        table_name = x['name']
        if('.yml' in table_name): 
            file_path = folder_path+'/'+table_name
            file_list.append(file_path)
            data = get_file_content(project, file_path,branch) 
            data_list.append(data)
    return file_list, data_list

space_trailing = '         '
def write_data_to_snowflake(snow, train_data, table_name="RARA_NLU_INTENT_UTTERANCES_TBL"): 
    query = f"""
    create or replace table {table_name}(
        INTENT varchar not null,
        UTTERANCE varchar,
        TRAINED boolean,
        INTENT_START_DATE TIMESTAMP_NTZ(9),
        INTENT_END_DATE TIMESTAMP_NTZ(9), 
        LOAD_DATE TIMESTAMP_NTZ(9)
    );
    """
    print(space_trailing + f"INFO: Create or replace the table: {table_name}")

    snow.execute_query(query)
    snow.write_data(train_data, table_name)
    print(space_trailing+f"INFO: Totally {len(train_data)} rows are written to Snowflake Table.")

def write_data_to_snowflake_lookup(snow, train_data, table_name): 
    query = f"""
    create or replace table {table_name}(
        LOOKUP varchar not null,
        WORD varchar,
        STATUS boolean,
        START_DATE TIMESTAMP_NTZ(9),
        END_DATE TIMESTAMP_NTZ(9), 
        LOAD_DATE TIMESTAMP_NTZ(9)
    );
    """
    print(space_trailing + f"INFO: Create or replace the table: {table_name}")

    snow.execute_query(query)
    snow.write_data(train_data, table_name)
    print(space_trailing+f"INFO: Totally {len(train_data)} rows are written to Snowflake Table.")

import pandas as pd 


def refresh_train_data(train_data1, prev_data1, load_date): 
    train_data = train_data1.copy()
    prev_data = prev_data1.copy()
    prev_data['source_db'] = 'snowflake'
    train_data['source_nlu'] = 'rasa' 
    train_data['TRAINED'] = True
    df = pd.merge(train_data,prev_data,on=['INTENT','UTTERANCE','TRAINED'],how='outer')  

    # detect the changes 
    change_trigger = False
    # new intent-utterances (nlu file has the data, but snowflake db does not)
    if(len(df[df.source_db.isnull()])>0): 
        df.loc[df.source_db.isnull(), 'TRAINED'] = True
        df.loc[df.source_db.isnull(), 'INTENT_START_DATE'] = str(load_date)
        df.loc[df.source_db.isnull(), 'INTENT_END_DATE'] = '9999-01-01' 
        change_trigger = True 

    # removed intent-utterances (nlu file does not have the data, but snowflake db have it)
    if(len(df[df.source_nlu.isnull()])>0): 
        df.loc[df.source_nlu.isnull(), 'TRAINED'] = False
        df.loc[df.source_nlu.isnull(), 'INTENT_END_DATE'] = str(load_date)
        change_trigger = True 
    
    return change_trigger, df 


def refresh_lookup_data(train_data1, prev_data1, load_date): 
    train_data = train_data1.copy()
    prev_data = prev_data1.copy()
    prev_data['source_db'] = 'snowflake'
    train_data['source_nlu'] = 'rasa' 
    train_data['STATUS'] = True
    df = pd.merge(train_data,prev_data,on=['LOOKUP','WORD','STATUS'],how='outer')  

    # detect the changes 
    change_trigger = False
    # new intent-utterances (nlu file has the data, but snowflake db does not)
    if(len(df[df.source_db.isnull()])>0): 
        df.loc[df.source_db.isnull(), 'STATUS'] = True
        df.loc[df.source_db.isnull(), 'START_DATE'] = str(load_date)
        df.loc[df.source_db.isnull(), 'END_DATE'] = '9999-01-01' 
        change_trigger = True 

    # removed intent-utterances (nlu file does not have the data, but snowflake db have it)
    if(len(df[df.source_nlu.isnull()])>0): 
        df.loc[df.source_nlu.isnull(), 'STATUS'] = False
        df.loc[df.source_nlu.isnull(), 'END_DATE'] = str(load_date)
        change_trigger = True 
    
    return change_trigger, df 